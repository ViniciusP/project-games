<!--<?php echo $_SESSION['Auth']['User']['id']; ?>-->
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Jogo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="jogos index large-9 medium-8 columns content">
    <h3><?= __('Jogos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th></th>
                <th><?= $this->Paginator->sort('descricao') ?></th>
                <th><?= $this->Paginator->sort('tipo') ?></th>
                <th><?= $this->Paginator->sort('preco') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($jogos as $jogo): ?>
            <tr>
                <td><?= $this->Number->format($jogo->id) ?></td>
                <td><?= h($jogo->nome) ?></td>
                <td><img src="<?php echo 'http://192.168.1.18/IMGS-GAMES/'.$jogo->identificador_foto ?>" /></td>
                <td><?= h($jogo->descricao) ?></td>
                <td><?= h($jogo->tipo) ?></td>
                <td><?= h($jogo->preco) ?></td>
                <!--<td><?= $jogo->has('user') ? $this->Html->link($jogo->user->id, ['controller' => 'Users', 'action' => 'view', $jogo->user->id]) : '' ?></td>-->
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $jogo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $jogo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $jogo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $jogo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
